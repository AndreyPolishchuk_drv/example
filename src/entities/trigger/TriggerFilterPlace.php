<?php
namespace app\src\entities\trigger;

use app\src\entities\AbstractModel;

/**
 * Class TriggerFilterPlace
 * @package app\src\entities\notification
 *
 * @property int $trigger_id
 * @property int $place_id
 * @property int $type
 */
class TriggerFilterPlace extends AbstractModel
{
    public static function tableName()
    {
        return 'trigger_filter_place';
    }

    public function rules()
    {
        return [
            [['trigger_id', 'place_id'], 'required'],
            [['trigger_id', 'place_id'], 'integer'],
        ];
    }

}